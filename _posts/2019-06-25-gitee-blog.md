---
layout: post
title: "坑爹代码 | 要不然你说手机号码应该怎么验证嘛！"
---

<p>在现如今的互联网应用中，手机号码已经完全取代电子邮件成为了用户注册账号的唯一标识。而随着移动网络的高速发展，各种前缀的手机号码不断推出，如何验证有效的手机号码就是程序员必须关注的功能点，你说重要吧？ 好像没那么重要，你说不重要吧？ 一旦程序出错，用户注册不了那就是大事！</p>

<p>所以你怎么看下面这段验证手机号码是否正确的代码呢？</p>

<pre>
<code class="language-java">public String validatePhone(String phoneStr) {
  if (phoneStr != null) {
	if (phoneStr.length() == 11) {
	  if (isNumeric(phoneStr)) {
		if (phoneStr.startsWith("130") || phoneStr.startsWith("131") || phoneStr.startsWith("132")
			|| phoneStr.startsWith("134") || phoneStr.startsWith("135") || phoneStr.startsWith("136")
			|| phoneStr.startsWith("137") || phoneStr.startsWith("138") || phoneStr.startsWith("139")
			|| phoneStr.startsWith("140") || phoneStr.startsWith("141") || phoneStr.startsWith("142")
			|| phoneStr.startsWith("144") || phoneStr.startsWith("145") || phoneStr.startsWith("146")
			|| phoneStr.startsWith("147") || phoneStr.startsWith("148") || phoneStr.startsWith("149")
			|| phoneStr.startsWith("150") || phoneStr.startsWith("151") || phoneStr.startsWith("152")
			|| phoneStr.startsWith("154") || phoneStr.startsWith("155") || phoneStr.startsWith("156")
			|| phoneStr.startsWith("157") || phoneStr.startsWith("158") || phoneStr.startsWith("159")
			|| phoneStr.startsWith("170") || phoneStr.startsWith("171") || phoneStr.startsWith("172")
			|| phoneStr.startsWith("174") || phoneStr.startsWith("175") || phoneStr.startsWith("176")
			|| phoneStr.startsWith("177") || phoneStr.startsWith("178") || phoneStr.startsWith("179")
			|| phoneStr.startsWith("180") || phoneStr.startsWith("181") || phoneStr.startsWith("182")
			|| phoneStr.startsWith("184") || phoneStr.startsWith("185") || phoneStr.startsWith("186")
			|| phoneStr.startsWith("187") || phoneStr.startsWith("188") || phoneStr.startsWith("189")) {
			return "手机号正确";
		  } else {
			return "手机号规则错误";
		  }
		} else {
		  return "手机号必须为数字";
		}
	} else {
		return "手机号长度必须为11位";
	}
  } else {
	return "手机号不能为空";
  }
}</code></pre>

<p>逻辑好像没有问题，但是真的好啰嗦啊！！！</p>

<p>那么你有更好的方法吗？</p>

<p>请移步下面链接发表评论，领取奖品：</p>

<p><a href="https://gitee.com/oschina/bullshit-codes/blob/master/java/PhoneRuleValidate.java">https://gitee.com/oschina/bullshit-codes/blob/master/java/PhoneRuleValidate.java</a></p>

<p>码云 6 周年，我们正在征集各种坑爹代码，很多奖品等你来拿</p>

<p>详细的参与方法请看&nbsp;&nbsp;<a href="https://gitee.com/oschina/bullshit-codes">https://gitee.com/oschina/bullshit-codes</a></p>

<p>------ 分割线 ------</p>

<p>其他坑爹代码吐槽：</p>

<ul>
	<li><a href="https://www.oschina.net/news/107032/gitee-bullshit-codes-stringbuffer" target="_blank">坑爹代码&nbsp;| 这样使用 StringBuffer 的方法有什么坑？</a></li>
	<li><a href="https://www.oschina.net/news/107081/gitee-bad-code-one-line-code" target="_blank">坑爹代码&nbsp;| 你写过的最长的一行代码有多长？？？</a></li>
	<li><a href="https://www.oschina.net/news/107151/nesting-bad-code">坑爹代码 | 循环+条件判断，你最多能嵌套几层？</a></li>
	<li><a href="https://www.oschina.net/news/107213/gitee-bullshit-codes-optimize-for-future">坑爹代码 | 为了后期优化查询速度 ~ 颇有商业头脑！</a></li>
	<li><a href="https://www.oschina.net/news/107315/gitee-bad-code-exception">坑爹代码 | 你是如何被异常玩然后变成玩异常的？</a></li>
	<li><a href="https://www.oschina.net/news/107345/gitee-bullshit-amazing-stream">坑爹代码 | Stream 玩得最 6 的代码，看过的人都惊呆了</a></li>
	<li><a href="https://www.oschina.net/news/107411/github-bullshit-code-with-go-defer">坑爹代码 | Go 语言的 defer 能制造出多少坑来？</a></li>
	<li><a href="https://www.oschina.net/news/107561/gitee-bullshit-code-java-logging">坑爹代码 | 这样的日志封装到底是坑爹还是有用呢？</a></li>
</ul>
