---
layout: post
title: "码云企业版任务管理增加了成员看板，谁干什么都清楚"
---

<p>码云企业版的任务管理又更新啦！！！</p><p>新增成员看板，让你可以清楚看到团队成员手头上的任务，如下图所示：</p><p><img alt="" src="https://static.oschina.net/uploads/space/2018/0606/185911_u7az_12.jpg"/></p><p>接下来我们还会继续改进看板，支持各种自定义，敬请期待。</p><p>同时还支持任务的批量操作：</p><p><img alt="" src="https://static.oschina.net/uploads/space/2018/0606/190020_HxMs_12.jpg"/></p><p>预告：上周我们推出了 JavaDoc 在线生成和托管服务（<a href="https://www.oschina.net/news/96659/gitee-add-javadoc-generator" target="_blank">详情</a>），非常受欢迎。但是还没结束，因为 PHP 文档在线生成和托管很快也将推出，还有。。。。还有很多其他的，敬请期待。</p><p>了解一下？ <a href="https://gitee.com/enterprises?from=oscnews4">https://gitee.com/enterprises</a></p>